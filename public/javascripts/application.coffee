app = angular.module('app', [ 'ui.codemirror' ])

app.controller 'ScriptController', ['$scope', '$http', ($scope, $http) ->

  console.log 'Script controller says hi'

  $scope.code = 'add 19, 26'

  @banana = (code) ->
    console.log 'this banana', code
    promise = $http.post '/code', {msg: code}
    promise.success = (data, status, headers, config) -> console.log 'Successfully posted some code'
    promise.error = (data, status, headers, config) -> console.log 'Darn, posting code failed abysmally', data, status, headers, config
]
