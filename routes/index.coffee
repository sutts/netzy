express = require 'express'
router = express.Router()
coffee = require 'coffee-script'

router.get '/', (req, res, next) ->
  res.render 'index', title: 'Express'
  
module.exports = router
